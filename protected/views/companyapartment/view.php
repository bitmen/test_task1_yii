<?php
$this->breadcrumbs=array(
	'Companyapartments'=>array('index'),
	$model->title,
);
?>

<h1>View Companyapartment #<?php echo $model->companyapartment_id; ?></h1>
<hr />
<?php 
$this->beginWidget('zii.widgets.CPortlet', array(
	'htmlOptions'=>array(
		'class'=>''
	)
));
$this->widget('bootstrap.widgets.TbMenu', array(
	'type'=>'pills',
	'items'=>array(
		array('label'=>'Create', 'icon'=>'icon-plus', 'url'=>Yii::app()->controller->createUrl('create'), 'linkOptions'=>array()),
                array('label'=>'List', 'icon'=>'icon-th-list', 'url'=>Yii::app()->controller->createUrl('index'), 'linkOptions'=>array()),
                array('label'=>'Update', 'icon'=>'icon-edit', 'url'=>Yii::app()->controller->createUrl('update',array('id'=>$model->companyapartment_id)), 'linkOptions'=>array()),
		//array('label'=>'Search', 'icon'=>'icon-search', 'url'=>'#', 'linkOptions'=>array('class'=>'search-button')),
		array('label'=>'Print', 'icon'=>'icon-print', 'url'=>'javascript:void(0);return false', 'linkOptions'=>array('onclick'=>'printDiv();return false;')),

)));
$this->endWidget();
?>
<div class='printableArea'>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'companyapartment_id',
		'discount',
		'discount_description',
		'weekday_from',
		'weekday_to',
		'holiday_from',
		'holiday_to',
		'event_time',
		'title',
		'info',
		'announcement',
		'has_logo',
		'public_state',
		'status',
		'is_popular',
		'decline_cause',
		'advert_type',
		'paid_before',
		'comment_count',
		'creation_time',
		'update_time',
		'f_category_id',
		'f_user_id',
		'paid_link',
		'utime',
		'auto_update',
		'date_auto_update',
		'last_auto_update',
	),
)); ?>
</div>
<style type="text/css" media="print">
body {visibility:hidden;}
.printableArea{visibility:visible;} 
</style>
<script type="text/javascript">
function printDiv()
{

window.print();

}
</script>
