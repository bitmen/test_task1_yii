<?php

/**
 * This is the model class for table "ur_type".
 *
 * The followings are the available columns in table 'ur_type':
 * @property integer $type_id
 * @property string $title
 * @property string $class_name
 * @property string $seo_title_pattern
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property string $seo_texttop
 * @property string $seo_textbottom
 *
 * The followings are the available model relations:
 * @property UrCategory[] $urCategories
 * @property UrCounter[] $urCounters
 */
class Type extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ur_type';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type_id, title', 'required'),
			array('type_id', 'numerical', 'integerOnly'=>true),
			array('title, class_name', 'length', 'max'=>45),
			array('seo_title_pattern', 'length', 'max'=>255),
			array('seo_title, seo_keywords', 'length', 'max'=>250),
			array('seo_description, seo_texttop, seo_textbottom', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('type_id, title, class_name, seo_title_pattern, seo_title, seo_description, seo_keywords, seo_texttop, seo_textbottom', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'urCategories' => array(self::HAS_MANY, 'UrCategory', 'f_type_id'),
			'urCounters' => array(self::HAS_MANY, 'UrCounter', 'f_type_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'type_id' => 'Type',
			'title' => 'Title',
			'class_name' => 'Class Name',
			'seo_title_pattern' => 'Seo Title Pattern',
			'seo_title' => 'Seo Title',
			'seo_description' => 'Seo Description',
			'seo_keywords' => 'Seo Keywords',
			'seo_texttop' => 'Seo Texttop',
			'seo_textbottom' => 'Seo Textbottom',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('type_id',$this->type_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('class_name',$this->class_name,true);
		$criteria->compare('seo_title_pattern',$this->seo_title_pattern,true);
		$criteria->compare('seo_title',$this->seo_title,true);
		$criteria->compare('seo_description',$this->seo_description,true);
		$criteria->compare('seo_keywords',$this->seo_keywords,true);
		$criteria->compare('seo_texttop',$this->seo_texttop,true);
		$criteria->compare('seo_textbottom',$this->seo_textbottom,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Type the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
