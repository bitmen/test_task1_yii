<?php

/**
 * This is the model class for table "ur_district".
 *
 * The followings are the available columns in table 'ur_district':
 * @property integer $district_id
 * @property string $title
 * @property integer $f_city_id
 * @property integer $_old_id
 *
 * The followings are the available model relations:
 * @property UrApartment[] $urApartments
 * @property UrCity $fCity
 * @property UrUser[] $urUsers
 */
class District extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ur_district';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, f_city_id', 'required'),
			array('f_city_id, _old_id', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('district_id, title, f_city_id, _old_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'urApartments' => array(self::HAS_MANY, 'UrApartment', 'f_district_id'),
			'fCity' => array(self::BELONGS_TO, 'UrCity', 'f_city_id'),
			'urUsers' => array(self::HAS_MANY, 'UrUser', 'f_district_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'district_id' => 'District',
			'title' => 'Title',
			'f_city_id' => 'F City',
			'_old_id' => 'Old',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('district_id',$this->district_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('f_city_id',$this->f_city_id);
		$criteria->compare('_old_id',$this->_old_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return District the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
